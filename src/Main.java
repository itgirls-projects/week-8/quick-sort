import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] words = {"strawberry", "honey", "apple", "pineapple", "banana", "orange", "grape"};
        System.out.println("Initial array: " + Arrays.toString(words));
        System.out.println("--------");
        quickSort(words, 0, words.length - 1);
        System.out.println("Sorted array: " + Arrays.toString(words));
    }

    public static void quickSort(String[] array, int lowIndex, int highIndex) {
        if (lowIndex >= highIndex){
            return;
        }
        // в качестве опорного элемента я беру последний - прочитала, что можно в целом брать любой, и с последним мне как-то проще получилось понять материал
        String pivot = array[highIndex];
        int leftPointer = lowIndex;
        int rightPointer = highIndex - 1;

        while(leftPointer < rightPointer) {
            while(leftPointer < rightPointer && array[leftPointer].charAt(0) < pivot.charAt(0)){
                leftPointer++;
            }
            while(leftPointer < rightPointer && array[rightPointer].charAt(0) > pivot.charAt(0)){
                rightPointer--;
            }
            swap(array, leftPointer, rightPointer);
        }
        swap(array, leftPointer, highIndex);

        quickSort(array, lowIndex, leftPointer - 1);
        quickSort(array, leftPointer + 1, highIndex);
    }

    public static void swap(String[] array, int leftPointer, int rightPointer){
        String temp = array[leftPointer];
        array[leftPointer] = array[rightPointer];
        array[rightPointer] = temp;
    }
}